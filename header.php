<?php
/**
 * Default Header Template
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <?php // Place favicon.ico and apple-touch-icon.png in the root of the domain ?>

    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="site-header" role="banner">
<div class="title-bar align-justify" data-responsive-toggle="responsive-menu" data-hide-for="medium">
<a class="site-title " href="<?php echo home_url( '/' ); ?>"><?php bloginfo( 'name' ); ?></a>
  <button class="" type="button" data-toggle="responsive-menu">
  <i  class=" fa fa-bars menu-hamburguer"></i>
  </button>
</div>

<div class="top-bar" id="responsive-menu" data-sticky data-options="marginTop:0;">
  <div class="top-bar-left align-middle">
  <a class="site-title hide-title"  href="<?php echo home_url( '/' ); ?>"><?php bloginfo( 'name' ); ?></a>
  </div>
  <div class="top-bar-right">
  <?php h5bs_primary_nav(); ?>
<?php if ( has_nav_menu( 'mobile' ) ) {  
    h5bs_mobile_nav(); 
}?>
    </ul>
  </div>
</div>

</div>
