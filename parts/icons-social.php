<div class="social-icons">
    <?php if ( get_option( 'client_twitter_url' ) ) { ?>
        <a href="<?php echo get_option( 'client_twitter_url' ); ?>" target="_blank"><i class="fab fa-twitter"></i></a>
    <?php } ?>
    <?php if ( get_option( 'client_facebook_url' ) ) { ?>
        <a href="<?php echo get_option( 'client_facebook_url' ); ?>" target="_blank"><i class="fab fa-facebook"></i></a>
    <?php } ?>
    <?php if ( get_option( 'client_google_url' ) ) { ?>
       <a href="<?php echo get_option( 'client_google_url' ); ?>" target="_blank"><i class="fab fa-google-plus"></i></a>
    <?php } ?>
    <?php if ( get_option( 'client_linkedin_url' ) ) { ?>
        <a href="<?php echo get_option( 'client_linkedin_url' ); ?>" target="_blank"><i class="fab fa-linkedin"></i></a>
    <?php } ?>
    <?php if ( get_option( 'client_instagram_url' ) ) { ?>
        <a href="<?php echo get_option( 'client_instagram_url' ); ?>" target="_blank"><i class="fab fa-instagram"></i></a>
    <?php } ?>
    <?php if ( get_option( 'client_pinterest_url' ) ) { ?>
        <a href="<?php echo get_option( 'client_pinterest_url' ); ?>" target="_blank"><i class="fab fa-pinterest"></i></a>
    <?php } ?>
    <?php if ( get_option( 'client_youtube_url' ) ) { ?>
        <a href="<?php echo get_option( 'client_youtube_url' ); ?>" target="_blank"><i class="fab fa-youtube"></i></a>
    <?php } ?>
</ul>
